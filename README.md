### Docker Android Client

We this app, you easily manage your docker containers. You'll need to deploy our backend before doing something.

Before building this app, you just need to add these 2 lines in `gradle.properties`:

```shell
ServerURL=<SERVER-URL>
RedisURL=<REDIS-URL>
```

Made with love by Mathieu Tortuyaux and Benjamin Varailhon de la Filolie
