package com.gitlab.tortuemat.docker;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.gitlab.tortuemat.docker.implementations.Api;

import java.lang.ref.WeakReference;
import java.util.concurrent.TimeUnit;

public class NewConnectionActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_connection);
        Button save_button = findViewById(R.id.save_button);
        save_button.setOnClickListener(
            new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    TextInputEditText container_name, image_name;
                    String container_name_str, image_name_str;

                    container_name = findViewById(R.id.container_name);
                    image_name = findViewById(R.id.image_name);
                    container_name_str = container_name.getText().toString();
                    image_name_str = image_name.getText().toString();

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                        new GrpcTask(NewConnectionActivity.this).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, image_name_str, container_name_str);
                    } else {
                        new GrpcTask(NewConnectionActivity.this).execute(image_name_str, container_name_str);
                    }

                }
        });
    }

    static class GrpcTask extends AsyncTask<String, Void, String> {

        private Api cli;
        private WeakReference<Activity> activityWeakReference;

        GrpcTask(Activity activity) {
            this.activityWeakReference = new WeakReference<>(activity);
            cli = new Api(BuildConfig.SERVER_URL, 9090);
        }

        @Override
        protected String doInBackground(String... args) {
            cli = new Api(BuildConfig.SERVER_URL, 9090);
            String container_name, image_name;
            container_name = args[1];
            image_name = args[0];
            return cli.createContainer(image_name, container_name);
        }

        @Override
        protected void onPostExecute(String str) {
            try {
                cli.getChannel().shutdown().awaitTermination(1, TimeUnit.SECONDS);
                activityWeakReference.get().finish();
            }catch(InterruptedException e) {
                Thread.currentThread().interrupt();
            }
        }
    }
}
