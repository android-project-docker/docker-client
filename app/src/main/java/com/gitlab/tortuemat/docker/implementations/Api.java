package com.gitlab.tortuemat.docker.implementations;

import android.util.Log;

import com.gitlab.tortuemat.docker.ApiGrpc;
import com.gitlab.tortuemat.docker.Container;
import com.gitlab.tortuemat.docker.Message;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;

public class Api {

    private ManagedChannel channel;
    private ApiGrpc.ApiBlockingStub stub;

    public ManagedChannel getChannel() {
        return channel;
    }

    public Api(String host, int port) {
        // create an api handler
        try {
            channel = ManagedChannelBuilder.forAddress(host, port).usePlaintext().build();
        } catch (Exception e) {
            Log.d("GRPC", String.format("unable to connect to server: %s", e.getMessage()));
        }
        stub = ApiGrpc.newBlockingStub(channel);

    }

    public String createContainer(String imageName, String containerName) {
        Container ctn = Container.newBuilder()
                .setImageName(imageName)
                .setContainerName(containerName)
                .build();
        return stub.createContainer(ctn).getMsg();
    }

    public String startContainer(String containerName) {
        Container ctn = Container.newBuilder()
                .setContainerName(containerName)
                .build();
        return stub.startContainer(ctn).getMsg();
    }

    public Iterator<Message> readLogs(String containerName) {
        Container ctn = Container.newBuilder()
                .setContainerName(containerName)
                .build();
        return stub.readLogs(ctn);
    }

    public Container[] getAllContainers() {
        List<Container> containers = new ArrayList<>();
        Iterator<Container> response = stub.getAllContainers(Message.newBuilder().build());
        while (response.hasNext()) {
            containers.add(response.next());
        }
        return containers.toArray(new Container[containers.size()]);
    }

    public String stopContainer(String containerName) {
        Container ctn = Container.newBuilder()
                .setContainerName(containerName)
                .build();
        return stub.stopContainer(ctn).getMsg();
    }

    public Container getContainer(String containerName) {
        return stub.getContainer(Container.newBuilder()
                .setContainerName(containerName)
                .build());
    }
}
