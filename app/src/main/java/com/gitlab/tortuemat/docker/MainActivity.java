package com.gitlab.tortuemat.docker;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.gitlab.tortuemat.docker.implementations.Api;
import com.gitlab.tortuemat.docker.notification.NotificationService;

import java.lang.ref.WeakReference;
import java.util.concurrent.TimeUnit;

public class MainActivity extends AppCompatActivity {

    private final int NEW_CONTAINER_RESULT = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        FloatingActionButton newConnectionButton = findViewById(R.id.new_connection);
        newConnectionButton.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent k = new Intent(MainActivity.this, NewConnectionActivity.class);
                        try {
                            startActivityForResult(k, NEW_CONTAINER_RESULT);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
        );
        new GrpcTask(this).execute();
        // start notification service
        startService(new Intent(this, NotificationService.class));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == NEW_CONTAINER_RESULT) {
            new GrpcTask(this).execute();
        }
    }

    @Override
    protected void onResume(){
        super.onResume();
        new GrpcTask(this).execute();
    }

    private class GrpcTask extends AsyncTask<Void, Void, Container[]> {

        private final WeakReference<Activity> activityReference;
        private Api cli;

        private GrpcTask(Activity activity) {
            this.activityReference = new WeakReference<>(activity);
        }

        @Override
        protected Container[] doInBackground(Void... voids) {
            cli = new Api(BuildConfig.SERVER_URL, 9090);
            return cli.getAllContainers();
        }

        @Override
        protected void onPostExecute(Container[] containers) {
            try {
                cli.getChannel().shutdown().awaitTermination(1, TimeUnit.SECONDS);
            }catch(InterruptedException e) {
                Thread.currentThread().interrupt();
            }

            Activity activity = activityReference.get();
            if (activity == null) {
                return;
            }


            final RecyclerView rv = findViewById(R.id.list_container);
            rv.setHasFixedSize(true);
            rv.setLayoutManager(new LinearLayoutManager(MainActivity.this));

            ContainerAdapter ca = new ContainerAdapter(MainActivity.this, containers);
            rv.setAdapter(ca);
        }
    }
}
