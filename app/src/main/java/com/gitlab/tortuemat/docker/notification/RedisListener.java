package com.gitlab.tortuemat.docker.notification;

import android.app.Service;
import android.content.Context;

import redis.clients.jedis.JedisPubSub;

public class RedisListener extends JedisPubSub {

    private NotificationHandler handler;
    private static final String CHANNEL_ID = "container-dead";

    RedisListener(Context context) {
        this.handler = new NotificationHandler(context, CHANNEL_ID, "container-dead", NotificationService.class);
    }

    @Override
    public void onMessage(String channel, String message) {
        super.onMessage(channel, message);
        handler.publish("docker", message, CHANNEL_ID);
    }
}