package com.gitlab.tortuemat.docker.notification;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.IBinder;
import android.support.annotation.Nullable;

import com.gitlab.tortuemat.docker.BuildConfig;

import java.lang.ref.WeakReference;

import redis.clients.jedis.Jedis;

public class NotificationService extends Service {
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        new JedisTask(this).execute();
        return Service.START_NOT_STICKY;
    }

    static class JedisTask extends AsyncTask<Void, Void, Void> {

        private WeakReference<Context> contextWeakReference;

        JedisTask(Context context) {
            this.contextWeakReference = new WeakReference<>(context);
        }

        @Override
        protected Void doInBackground(Void... voids) {
            Jedis cli = new Jedis(BuildConfig.REDIS_URL, 6379);
            cli.subscribe(new RedisListener(contextWeakReference.get()), "container-dead");
            return null;
        }
    }
}
