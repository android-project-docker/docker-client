package com.gitlab.tortuemat.docker;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.gitlab.tortuemat.docker.implementations.Api;

import java.lang.ref.WeakReference;
import java.util.concurrent.TimeUnit;

public class ContainerActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_container);

        Intent intent = getIntent();

        final String containerName = intent.getStringExtra("container_name");

        TextView logTextView = findViewById(R.id.log_text_view);
        logTextView.setMovementMethod(new ScrollingMovementMethod());

        Button button_start_container = findViewById(R.id.button_start_container);
        button_start_container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                    new StartContainerTask(ContainerActivity.this).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, containerName);
                } else {
                    new StartContainerTask(ContainerActivity.this).execute(containerName);
                }
            }
        });

        Button button_stop_container = findViewById(R.id.button_stop_container);
        button_stop_container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                    new StopContainerTask(ContainerActivity.this).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, containerName);
                } else {
                    new StopContainerTask(ContainerActivity.this).execute(containerName);
                }
            }
        });

        new GetContainerInfoTask(this).execute(containerName);
    }

    static class GetContainerInfoTask extends AsyncTask<String, Void, Container> {

        private WeakReference<Activity> activityWeakReference;

        GetContainerInfoTask(Activity activity) {
            this.activityWeakReference = new WeakReference<>(activity);
        }

        @Override
        protected Container doInBackground(String... strings) {
            String containerName = strings[0];
            Api api = new Api(BuildConfig.SERVER_URL, 9090);
            return api.getContainer(containerName);
        }

        @Override
        protected void onPostExecute(final Container container) {
            final Activity activity = activityWeakReference.get();
            if (activity == null) {
                return;
            }
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    activity.setTitle(container.getContainerName());

                    TextView status = activity.findViewById(R.id.status);
                    status.setText(container.getStatus());

                    TextView imageName = activity.findViewById(R.id.image_name_display);
                    imageName.setText(container.getImageName());
                }
            });
        }
    }

    static class StartContainerTask extends AsyncTask<String, Void, String> {

        private WeakReference<Activity> activityWeakReference;
        private Api cli;

        StartContainerTask(Activity activity) {
            this.activityWeakReference = new WeakReference<>(activity);
            cli = new Api(BuildConfig.SERVER_URL, 9090);

        }

        @Override
        protected String doInBackground(String... strings) {
            String container = strings[0];
            Api api = new Api(BuildConfig.SERVER_URL, 9090);
            return api.startContainer(container);
        }

        @Override
        protected void onPostExecute(String res) {
            try {
                cli.getChannel().shutdown().awaitTermination(1, TimeUnit.SECONDS);
            } catch (InterruptedException e) {
                Log.e("GRPC-ERROR", "unable to close channel: "+e.getMessage());
            }
            Activity activity = activityWeakReference.get();
            Toast.makeText(activity, res, Toast.LENGTH_LONG).show();
        }
    }

    static class StopContainerTask extends AsyncTask<String, Void, String> {

        private WeakReference<Activity> activityWeakReference;
        private Api cli;

        StopContainerTask(Activity activity) {
            this.activityWeakReference = new WeakReference<>(activity);
            cli = new Api(BuildConfig.SERVER_URL, 9090);

        }

        @Override
        protected String doInBackground(String... strings) {
            String container = strings[0];
            Api api = new Api(BuildConfig.SERVER_URL, 9090);
            return api.stopContainer(container);
        }

        @Override
        protected void onPostExecute(String res) {
            try {
                cli.getChannel().shutdown().awaitTermination(1, TimeUnit.SECONDS);
            } catch (InterruptedException e) {
                Log.e("GRPC-ERROR", "unable to close channel: "+e.getMessage());
            }
            Activity activity = activityWeakReference.get();
            Toast.makeText(activity, res, Toast.LENGTH_LONG).show();
        }
    }

}
