package com.gitlab.tortuemat.docker;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class ContainerAdapter extends RecyclerView.Adapter<ContainerAdapter.ContainerViewHolder> {

    private Container[] containers;
    private Context context;

    public ContainerAdapter(Context context, Container[] containers){
        this.context = context;
        this.containers = containers;
    }

    @Override
    public int getItemCount() {
        return containers.length;
    }

    @Override
    public ContainerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.container_list_cell, parent, false);
        return new ContainerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ContainerViewHolder holder, int position) {
        Container ctn = containers[position];
        holder.display(ctn);
    }

    public class ContainerViewHolder extends RecyclerView.ViewHolder {

        private final TextView imageName;
        private final TextView containerName;
        private final TextView status;

        private Container currentCtn;

        public ContainerViewHolder(final View itemView){
            super(itemView);

            imageName = itemView.findViewById(R.id.list_image_name);
            containerName = itemView.findViewById(R.id.list_container_name);
            status = itemView.findViewById(R.id.list_status);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, ContainerActivity.class);
                    intent.putExtra("container_name", currentCtn.getContainerName());
                    try{
                        view.getContext().startActivity(intent);
                    } catch (Exception e){
                        e.printStackTrace();
                    }
                }
            });
        }

        private void display(Container ctn){
            currentCtn = ctn;
            imageName.setText(ctn.getImageName());
            containerName.setText(ctn.getContainerName());
            status.setText(ctn.getStatus());
        }

    }
}
